#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

using namespace cv;
using namespace std;

Mat src, src_gray, test, test2;
int thresh = 100;
int max_thresh = 255;
int flag = 0;
RNG rng(12345);
vector<vector<Point> > contours;
vector<vector<Rect> > reigions;
vector<float> intercontour_distances;
float contour_center, distance_origin, distance_center, contour_size;
Point cloth_center;
Mat ROI1, ROI2, ROI3, ROI4;
Mat tROI1, tROI2, tROI3, tROI4;
Mat hsv, thsv;
vector<Point2f> mc;
vector<Moments> mu(contours.size());
Point minx,maxx,miny,maxy;

/// Function header
void thresh_callback(int, void*);
void euc_distance(vector<vector<Point> > contours, Mat drawing);
Point find_cloth_center();
void find_angle(vector<vector<Point> > contours);
void crop_ROI(vector<vector<Point> > contours);
void preprocessing();
void extract_color_hisogram(vector<vector<Point> > contours, Mat test);
void color_moments(vector<Mat> rects);
void find_hu_moments(vector<vector<Point> > contours);
bool find_minx(Point &p1, Point &p2);
bool find_maxx(Point &p1, Point &p2);
bool find_miny(Point &p1, Point &p2);
bool find_maxy(Point &p1, Point &p2);
void find_news(vector<vector<Point> > contours);

/** @function main */
int main(int argc, char** argv)
{
	/// Load source image and convert it to gray
	src = imread(argv[1], 1);
	//test = imread(argv[2]);
	//test2 = imread(argv[3]);

	/// Convert image to gray and blur it
	cvtColor(src, src_gray, CV_BGR2GRAY);
	blur(src_gray, src_gray, Size(3, 3));

	/// Create Window
	char* source_window = "Source";
	namedWindow(source_window, WINDOW_NORMAL);
	imshow(source_window, src);

	cloth_center = find_cloth_center();

	//cout<<"\n valu " << cbrt(-8.0);

	createTrackbar(" Canny thresh:", "Source", &thresh, max_thresh, thresh_callback);
	thresh_callback(0, 0);



	waitKey(0);
	return(0);
}

/** @function thresh_callback */
void thresh_callback(int, void*)
{
	Mat canny_output;

	vector<Vec4i> hierarchy;



	/// Detect edges using canny
	Canny(src_gray, canny_output, thresh, thresh * 2, 3);
	/// Find contours
	findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	vector<double> distances((contours.size() / 2)*(contours.size() - 1));

	/// Get the moments

	for (int i = 0; i < contours.size(); i++)
	{
		mu.push_back(moments(contours[i], false));
	}

	///  Get the mass centers:
	
	for (int i = 0; i < contours.size(); i++)
	{
		mc.push_back( Point2f(mu[i].m10 / mu[i].m00, mu[i].m01 / mu[i].m00));
	}

	/// Draw contours
	Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);

	printf("\t Info: Area and Contour Length \n");
	for (int i = 0; i < contours.size(); i++)
	{
		if (i % 2 == 0) {
			printf(" \n* Contour[%d] - Area (M_00) = %.2f - Area OpenCV: %.2f - Length: %.2f", i, mu[i].m00, contourArea(contours[i]), arcLength(contours[i], true));
			cout << " - center:" << mc[i];
			Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, Point());
			circle(drawing, mc[i], 4, color, -1, 8, 0);
		}
	}
	euc_distance(contours, drawing);
	cout << CV_VERSION;

	find_hu_moments(contours);
	find_news(contours);

	
	circle(drawing, minx, 4, Scalar(255,0,0), -1, 8, 0);
	circle(drawing, miny, 4, Scalar(255, 0, 0), -1, 8, 0);
	circle(drawing, maxx, 4, Scalar(255, 0, 0), -1, 8, 0);
	circle(drawing, maxy, 4, Scalar(255, 0, 0), -1, 8, 0);
	/// Show in a window
	namedWindow("Contours", WINDOW_NORMAL);
	imshow("Contours", drawing);
	
	//preprocessing();
	//find_angle(contours);
	//crop_ROI(contours);

	//cout << "\n original -------------";
	//color_moments({ ROI1,ROI2,ROI3,ROI4 });
	//extract_color_hisogram(contours, test);

	//cout << "\n test == original -------------";
	//color_moments({ tROI1,tROI2,tROI3,tROI4 });
	//extract_color_hisogram(contours, test2);

	//cout << "\n wrong tests -------------";
	//color_moments({ tROI1,tROI2,tROI3,tROI4 });



}

///** @function euc_distance*/
void euc_distance(vector<vector<Point> > contours, Mat drawing) {

	for (int i = 0; i < contours.size(); i++) {

		cout << "\n---------------------------------------contour " << i;

		// use contours[i] for the current contour
		if (i % 2 == 0) {  // **************************************************************************************************change larer
			//******************************************************************************************************************
			
				distance_origin = sqrt(pow((0 - mc[i].x), 2) + pow((0 - mc[i].y), 2));  // distance to the contour center from (0,0)
				distance_center = sqrt(pow((cloth_center.x - mc[i].x), 2) + pow((cloth_center.y - mc[i].y), 2));  // distance to the contour center from cloth center


				cout << "\n" << distance_center << " " << distance_origin;
				//cout << "\n" << sqrt(pow((0 - contours[i][j].x), 2) + pow((0 - contours[i][j].y), 2));
				//line(drawing, Point(0, 0), contours[2][j], Scalar(255, 255, 255), 1, 8, 0);

			
		}

	}
}


//find the center of the cloth
Point find_cloth_center() {

	Mat thr;
	threshold(src_gray, thr, 100, 255, THRESH_BINARY);

	Mat thrCopy = thr.clone();

	int x_min = thr.cols, x_max = 0, y_min = thr.rows, y_max = 0;
	int r, c;
	for (r = 0; r < thr.rows; ++r)
	{
		for (c = 0; c < thr.cols; ++c)
		{
			if (thr.at<uchar>(r, c) == 0)
			{
				if (r < y_min)
					y_min = r;
				if (r > y_max)
					y_max = r;
				if (c < x_min)
					x_min = c;
				if (c > x_max)
					x_max = c;
			}
		}
	}



	cv::Point middle((int)(x_min + (x_max - x_min) / 2), (int)(y_min + (y_max - y_min) / 2));
	return middle;
}

//find boundingboxes of the contours
void find_angle(vector<vector<Point> > contours) {

	Mat threshold_output;
	vector<vector<Point> > contours_poly(contours.size());
	vector<RotatedRect> minRect(contours.size());
	vector<Point2f>center(contours.size());
	vector<float>radius(contours.size());
	Point p1, p2, p3, p4;

	for (int i = 0; i < contours.size(); i++)
	{
		if (i % 2 == 0) {
			minRect[i] = minAreaRect(Mat(contours[i]));
		}
	}
	threshold(src_gray, threshold_output, thresh, 255, THRESH_BINARY);

	/// Draw polygonal contour + bonding rects + circles

	//---------------------------------------------------------------------------check the documentation for angle
	///https://namkeenman.wordpress.com/2015/12/18/open-cv-determine-angle-of-rotatedrect-minarearect/????????????????????????
	Mat drawing = Mat::zeros(threshold_output.size(), CV_8UC3);
	for (int i = 0; i < contours.size(); i++)
	{
		if (i % 2 == 0) {
			Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			// contour
			drawContours(drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point());
			// rotated rectangle
			Point2f rect_points[4]; minRect[i].points(rect_points);
			/*for (int j = 0; j < 4; j++)
				line(drawing, rect_points[j], rect_points[(j + 1) % 4], color, 1, 8);*/


				//rectangle(drawing, rect_points[1], rect_points[3], Scalar(255, 255, 255), -1);

			p1 = Point(((int)(rect_points[0].x + rect_points[1].x) / 2), ((int)(rect_points[0].y + rect_points[1].y) / 2));
			p2 = Point(((int)(rect_points[1].x + rect_points[2].x) / 2), ((int)(rect_points[1].y + rect_points[2].y) / 2));
			p3 = Point(((int)(rect_points[2].x + rect_points[3].x) / 2), ((int)(rect_points[2].y + rect_points[3].y) / 2));
			p4 = Point(((int)(rect_points[3].x + rect_points[0].x) / 2), ((int)(rect_points[3].y + rect_points[0].y) / 2));
			Point ctr = (rect_points[1] + rect_points[3])*0.5;

			vector<Rect> rects{ Rect(p1,p4) ,Rect(rect_points[1],ctr) ,Rect(p2, p3) ,Rect(ctr,rect_points[3]) };
			reigions.push_back(rects);

			//reigions[i][0] = Rect(p1,p4);
			//reigions[i][1] = Rect(rect_points[1],ctr);
			//reigions[i][2] = Rect(p2, p3);
			//reigions[i][3] = Rect(ctr,rect_points[3]);


			double myContourAngle = minRect[i].angle;
			if (minRect[i].size.width < minRect[i].size.height) {
				myContourAngle = myContourAngle - 90;
			}

			cout << "\n angle :" << myContourAngle;
		}
	}

	/// Show in a window
	namedWindow("Contours", CV_WINDOW_AUTOSIZE);
	imshow("Contours", drawing);
}


//crop ROI
void crop_ROI(vector<vector<Point> > contours) {


	for (int i = 0; i < contours.size(); i++) {
		if (i % 2 == 0) {
			ROI1 = hsv(reigions[i][0]);
			ROI2 = hsv(reigions[i][1]);
			ROI3 = hsv(reigions[i][2]);
			ROI4 = hsv(reigions[i][3]);
		}
	}



	namedWindow("R1", CV_WINDOW_AUTOSIZE);
	namedWindow("R2", CV_WINDOW_AUTOSIZE);
	namedWindow("R3", CV_WINDOW_AUTOSIZE);
	namedWindow("R4", CV_WINDOW_AUTOSIZE);
	namedWindow("Hsv", CV_WINDOW_AUTOSIZE);

	imshow("R1", ROI1);
	imshow("R2", ROI2);
	imshow("R3", ROI3);
	imshow("R4", ROI4);
	imshow("Hsv", hsv);
}

//extract color info
void extract_color_hisogram(vector<vector<Point> > contours, Mat test) {

	cvtColor(test, thsv, COLOR_BGR2HSV);



	int h_bins = 13, s_bins = 4, v_bins = 4;
	int histSize[] = { h_bins, s_bins ,v_bins };
	// hue varies from 0 to 179, saturation from 0 to 255
	float h_ranges[] = { 0, 13,23,38,48,73,83,98,108,133,143,151,168,180 };
	float s_ranges[] = { 0, 63.75,127.5,191.25,255 };
	float v_ranges[] = { 0, 63.75,127.5,191.25,255 };
	const float* ranges[] = { h_ranges, s_ranges,v_ranges };
	// Use the 0-th and 1-st channels
	int channels[] = { 0, 1 };

	Mat HROI1, HROI2, HROI3, HROI4;

	calcHist(&ROI1, 1, channels, Mat(), HROI1, 2, histSize, ranges, false, false);
	normalize(HROI1, HROI1, 0, 1, NORM_MINMAX, -1, Mat());
	calcHist(&ROI2, 1, channels, Mat(), HROI2, 2, histSize, ranges, false, false);
	normalize(HROI2, HROI2, 0, 1, NORM_MINMAX, -1, Mat());
	calcHist(&ROI3, 1, channels, Mat(), HROI3, 2, histSize, ranges, false, false);
	normalize(HROI3, HROI3, 0, 1, NORM_MINMAX, -1, Mat());
	calcHist(&ROI4, 1, channels, Mat(), HROI4, 2, histSize, ranges, false, false);
	normalize(HROI4, HROI4, 0, 1, NORM_MINMAX, -1, Mat());

	for (int i = 0; i < contours.size(); i++) {
		if (i % 2 == 0) {
			tROI1 = thsv(reigions[i][0]);
			tROI2 = thsv(reigions[i][1]);
			tROI3 = thsv(reigions[i][2]);
			tROI4 = thsv(reigions[i][3]);
		}
	}

	Mat tHROI1, tHROI2, tHROI3, tHROI4;

	calcHist(&tROI1, 1, channels, Mat(), tHROI1, 2, histSize, ranges, false, false);
	normalize(tHROI1, tHROI1, 0, 1, NORM_MINMAX, -1, Mat());
	calcHist(&tROI2, 1, channels, Mat(), tHROI2, 2, histSize, ranges, false, false);
	normalize(tHROI2, tHROI2, 0, 1, NORM_MINMAX, -1, Mat());
	calcHist(&tROI3, 1, channels, Mat(), tHROI3, 2, histSize, ranges, false, false);
	normalize(tHROI3, tHROI3, 0, 1, NORM_MINMAX, -1, Mat());
	calcHist(&tROI4, 1, channels, Mat(), tHROI4, 2, histSize, ranges, false, false);
	normalize(tHROI4, tHROI4, 0, 1, NORM_MINMAX, -1, Mat());

	for (int compare_method = 0; compare_method < 4; compare_method++)
	{
		double tl_tl = compareHist(HROI2, tHROI2, compare_method);
		double tr_tr = compareHist(HROI3, tHROI3, compare_method);
		double bl_bl = compareHist(HROI1, tHROI1, compare_method);
		double br_br = compareHist(HROI4, tHROI4, compare_method);
		cout << "\nIntersection : " << compare_method << " tl_tl, tr_tr, bl_bl, br_br : "
			<< tl_tl << " / " << tr_tr << " / " << bl_bl << " / " << br_br << endl;

	}

}

//find color moments
void color_moments(vector<Mat> rects) {

	double avgHue, avgSat, avgVal, varHue, varSat, varVal, skewHue, skewSat, skewVal;
	double sumHue, sumSat, sumVal;


	cv::Mat mean;
	cv::Mat stddev;

	double meanHue;

	for (int r = 0; r < rects.size(); r++) {
		int imgSize = rects[r].rows *rects[r].cols;

		cv::meanStdDev(rects[r], mean, stddev);

		avgHue = mean.at<double>(0, 0);
		avgSat = mean.at<double>(1, 0);
		avgVal = mean.at<double>(2, 0);
		varHue = stddev.at<double>(0, 0);
		varSat = stddev.at<double>(1, 0);
		varVal = stddev.at<double>(2, 0);

		/*cout << "\nreigion -----------------" << r << " \navgHue :" << avgHue << " \navgSat " << avgSat << " \navgVal " << avgVal
		<<"\nsatHue "	<< varHue << " \nsatSat " << varSat << " \nsatVal " <<varVal;*/

		sumHue = 0; sumSat = 0; sumVal = 0;


		for (int i = 0; i < rects[r].rows; i++) {
			for (int j = 0; j < rects[r].cols; j++) {

				sumHue += pow((rects[r].at<Vec3b>(i, j)[0] - avgHue), 3);
				sumSat += pow((rects[r].at<Vec3b>(i, j)[1] - avgSat), 3);
				sumVal += pow((rects[r].at<Vec3b>(i, j)[2] - avgVal), 3);

			}
		}

		cout << "\nSum HUe" << sumHue;
		cout << "\nSum Sat" << sumSat / imgSize;
		cout << "\nSum Val" << sumVal;
		skewHue = cbrt(sumHue / imgSize);
		skewSat = cbrt(sumSat / imgSize);
		skewVal = cbrt(sumVal / imgSize);

		cout << " total " << imgSize;

		cout << "\nreigion ----" << r << " \navgHue :" << avgHue << " \navgSat " << avgSat << " \navgVal " << avgVal;
		cout << " \nvarHue " << varHue << " \nvarSat " << varSat << " \nvarVal " << varVal;
		cout << " \nskewHue " << skewHue << " \nskewSat " << skewSat << " \nskewVal " << skewVal;
	}
}

//preprocessing
void preprocessing() {

	cvtColor(src, hsv, COLOR_BGR2HSV);

	//double r,g,b,cmax,cmin,delta;
	////convertion bgr to hsv
	//for (int i = 0; i < img.rows; i++) {
	//	for (int j = 0; j < img.cols; j++) {

	//		r = img.at<cv::Vec3b>(i, j)[2]/255;
	//		g = img.at<cv::Vec3b>(i, j)[1]/255;
	//		b = img.at<cv::Vec3b>(i, j)[0]/255;

	//		cmax = max(r,g);
	//		cmax = max(cmax, b);

	//		cmin = min(r,g);
	//		cmin = min(cmax,b);

	//		delta = cmax - cmin;

	//		// hue
	//		if (cmax = r) {
	//			img.at<cv::Vec3b>(i, j)[0] = 60 * ((g-b)/delta)%6;


	//		}

	//	}
	//}

}


//quantization
void quantize(Mat img) {
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {

		}
	}
}


// find hu moments
void find_hu_moments(vector<vector<Point> > contours) {
	
	for (int i = 0; i < contours.size();i++) {
		if (i % 2 == 0) {
				

				// Calculate Hu Moments
				double huMoments[7];
				HuMoments(mu[i], huMoments);

				// Log scale hu moments
				for (int j = 0; j < 7; j++)
				{
					huMoments[j] = -1 * copysign(1.0, huMoments[j]) * log10(abs(huMoments[j]));
					cout <<"\n contour "<<i<< " HU" << j << " : " << huMoments[j];
				}


			
		}
	}
}

//find news points
void find_news(vector<vector<Point> > contours) {
	


	for (int i = 0; i < contours.size();i++) {
		 minx = *min_element(contours[i].begin(), contours[i].end(), find_minx);
		 miny = *min_element(contours[i].begin(), contours[i].end(), find_miny);
		 maxx = *min_element(contours[i].begin(), contours[i].end(), find_maxx);
		 maxy = *min_element(contours[i].begin(), contours[i].end(), find_maxy);


	}

	cout << "\nminx "<<minx <<" miny "<<miny <<" maxx "<<maxx<<" maxy "<<maxy ;
}

bool find_minx(Point &p1, Point &p2)
{
	return p1.x < p2.x;
	
}

bool find_miny(Point &p1, Point &p2)
{
	return p1.y < p2.y;

}

bool find_maxx(Point &p1, Point &p2)
{
	return p1.x > p2.x;

}

bool find_maxy(Point &p1, Point &p2)
{
	return p1.y > p2.y;

}