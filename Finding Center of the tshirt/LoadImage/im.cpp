#include "cv.h" //main opencv header
#include "highgui.h" //GUI header
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{

	Mat image;
	Mat gray,thr;
	Mat clone;
	int value;
	int pixels[256] = { 0 };
	double prob[256], cprob[256] = { 0 };

	if (argc != 2) {
		cout << "Two arguments are needed" << endl;
		return -1;
	}

	image = imread(argv[1], CV_BGR2GRAY);
	Mat copy = image.clone();

	cvtColor(image, gray, CV_BGR2GRAY); //perform gray scale conversion.

	threshold(gray, thr, 100, 255, THRESH_BINARY);

	Mat thrCopy = thr.clone();

	int x_min = thr.cols, x_max = 0, y_min = thr.rows, y_max = 0;
	int r, c;
	for (r = 0; r < thr.rows; ++r)
	{
		for (c = 0; c < thr.cols; ++c)
		{
			if (thr.at<uchar>(r, c) == 0)
			{
				if (r < y_min)
					y_min = r;
				if (r > y_max)
					y_max = r;
				if (c < x_min)
					x_min = c;
				if (c > x_max)
					x_max = c;
			}
		}
	}

	Point topLeft = Point(x_min, y_min);
	Point bottomLeft = Point(x_min, y_max);
	Point topRight = Point(x_max, y_min);
	Point bottomRight = Point(x_max, y_max);


	line(copy, topLeft, topRight, Scalar(255, 0, 0), 1, 8, 0);
	line(copy, topLeft, bottomLeft, Scalar(255, 0, 0), 1, 8, 0);
	line(copy, topRight, bottomRight, Scalar(255, 0, 0), 1, 8, 0);
	line(copy, bottomLeft, bottomRight, Scalar(255, 0, 0), 1, 8, 0);


	cv::Point middle((int)(x_min + (x_max - x_min) / 2), (int)(y_min + (y_max - y_min) / 2));
	cout << middle;

	Point pt1 = middle;
	Point pt2 = Point(0,0);
	line(thrCopy, pt1, pt2, Scalar(0), 1, 8, 0);


	

	namedWindow("image", WINDOW_AUTOSIZE); //original image
	imshow("image", image);

	namedWindow("Binarized Image", WINDOW_AUTOSIZE); //binarized image
	imshow("Binarized Image", thr);

	namedWindow("image with BB", WINDOW_AUTOSIZE); //with bounding box
	imshow("image with BB", copy);

	namedWindow("center", WINDOW_AUTOSIZE); //center of the garment
	imshow("center", thrCopy);

	waitKey(0);
	return 0;
}