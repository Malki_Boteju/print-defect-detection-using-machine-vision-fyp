#include "cv.h" //main opencv header
#include "highgui.h" //GUI header
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{

	Mat image;
	Mat gray, thr;
	Mat clone;
	int value;
	int pixels[256] = { 0 };
	double prob[256], cprob[256] = { 0 };

	if (argc != 2) {
		cout << "Two arguments are needed" << endl;
		return -1;
	}

	image = imread(argv[1], CV_BGR2GRAY);
	Mat copy = image.clone();

	cvtColor(image, gray, CV_BGR2GRAY); //perform gray scale conversion.

	threshold(gray, thr, 100, 255, THRESH_BINARY);

	Mat thrCopy = thr.clone();

	int x_min = thr.cols, x_max = 0, y_min = thr.rows, y_max = 0;
	int r, c;
	for (r = 0; r < thr.rows; ++r)
	{
		for (c = 0; c < thr.cols; ++c)
		{
			if (thr.at<uchar>(r, c) == 0)
			{
				if (r < y_min)
					y_min = r;
				if (r > y_max)
					y_max = r;
				if (c < x_min)
					x_min = c;
				if (c > x_max)
					x_max = c;
			}
		}
	}

	cv::Mat imgTranslated(image.size(), image.type(), cv::Scalar::all(255));
	image(cv::Rect(x_min, y_min, image.cols - x_min, image.rows - y_min)).copyTo(imgTranslated(cv::Rect(0, 0, image.cols - x_min, image.rows - y_min)));

	cvtColor(imgTranslated, imgTranslated, CV_BGR2GRAY); //perform gray scale conversion.

	threshold(imgTranslated, imgTranslated, 100, 255, THRESH_BINARY);


	int xmin = imgTranslated.cols, xmax = 0, ymin = imgTranslated.rows, ymax = 0;
	int rw, cl;
	for (rw = 0; rw < imgTranslated.rows; ++rw)
	{
		for (cl = 0; cl < imgTranslated.cols; ++cl)
		{
			if (imgTranslated.at<uchar>(rw, cl) == 0)
			{
				if (rw < ymin)
					ymin = rw;
				if (rw > ymax)
					ymax = rw;
				if (cl < xmin)
					xmin = cl;
				if (cl > xmax)
					xmax = cl;
			}
		}
	}

	Point topLeft = Point(x_min, y_min);
	Point bottomLeft = Point(x_min, y_max);
	Point topRight = Point(x_max, y_min);
	Point bottomRight = Point(x_max, y_max);


	line(copy, topLeft, topRight, Scalar(255, 0, 0), 1, 8, 0);
	line(copy, topLeft, bottomLeft, Scalar(255, 0, 0), 1, 8, 0);
	line(copy, topRight, bottomRight, Scalar(255, 0, 0), 1, 8, 0);
	line(copy, bottomLeft, bottomRight, Scalar(255, 0, 0), 1, 8, 0);


	cv::Point middle((int)(xmin + (xmax - xmin) / 2), (int)(ymin + (ymax - ymin) / 2));
	cout << middle;

	Point pt1 = middle;
	Point pt2 = Point(0, 0);
	line(imgTranslated, pt1, pt2, Scalar(0), 1, 8, 0);

	float otoc = sqrt((0-middle.x)*(0 - middle.x) + (0-middle.y)*(0 - middle.y));
	cout << "\n Origin to Cemter distance " <<otoc;

	namedWindow("image", WINDOW_AUTOSIZE); //original image
	imshow("image", image);


	namedWindow("Shift Center", WINDOW_AUTOSIZE);
	imshow("Shift Center", imgTranslated);

	waitKey(0);
	return 0;
}

