#include "cv.h" //main opencv header
#include "highgui.h" //GUI header
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{

	Mat image;
	Mat gray,thr;
	Mat clone;
	int value;
	int pixels[256] = { 0 };
	double prob[256], cprob[256] = { 0 };

	if (argc != 2) {
		cout << "Two arguments are needed" << endl;
		return -1;
	}

	image = imread(argv[1], CV_BGR2GRAY);

	cvtColor(image, gray, CV_BGR2GRAY); //perform gray scale conversion.

	threshold(gray, thr, 100, 255, THRESH_BINARY);

	int x_min = thr.cols, x_max = 0, y_min = thr.rows, y_max = 0;
	int xmin = thr.cols, xmax = 0, ymin = thr.rows, ymax = 0;
	int r, c;
	for (r = 0; r < thr.rows; ++r)
	{
		for (c = 0; c < thr.cols; ++c)
		{
			if (thr.at<uchar>(r, c) == 0)
			{
				if (r < y_min)
					y_min = r;
				if (r > y_max)
					y_max = r;
				if (c < x_min)
					x_min = c;
				if (c > x_max)
					x_max = c;
			}
			else {
			
				if (r < ymin)
					ymin = r;
				if (r > ymax)
					ymax = r;
				if (c < xmin)
					xmin = c;
				if (c > xmax)
					xmax = c;
			
			}
		}
	}

	


	cv::Point middle((int)(x_min + (x_max - x_min) / 2), (int)(y_min + (y_max - y_min) / 2));
	cout << "Garment Center"<< middle;

	cv::Point middleWindow((int)(xmin + (xmax - xmin) / 2), (int)(ymin + (ymax - ymin) / 2));
	cout << "\nImage Center" <<middleWindow;

	Point pt1 = middle;
	Point pt2 = Point(0,0);
	line(image, pt1, pt2, Scalar(0 ,0,0), 1, 8, 0);

	Point pt3 = middleWindow;
	line(image, pt3, pt2, Scalar(255 ,0,0), 1, 8, 0);


	//namedWindow("Garment Center", WINDOW_AUTOSIZE);
	//imshow("Garment Center", thr);

	//namedWindow("Window Center", WINDOW_AUTOSIZE);
	//imshow("Window Center", image);

	namedWindow("Centers", WINDOW_AUTOSIZE);
	imshow("Centers", image);


	waitKey(0);
	return 0;
}