#include "cv.h" //main opencv header
#include "highgui.h" //GUI header
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{

	Mat src = imread(argv[1]);
	Mat thr, dst,gray;

	cvtColor(src,gray,COLOR_RGB2GRAY);
	threshold(gray, thr, 100, 255, THRESH_BINARY_INV);
	imshow("thr", thr);

	std::vector<cv::Point> points;
	cv::Mat_<uchar>::iterator it = thr.begin<uchar>();
	cv::Mat_<uchar>::iterator end = thr.end<uchar>();
	for (; it != end; ++it)
		if (*it)
			points.push_back(it.pos());



	cv::RotatedRect box = cv::minAreaRect(cv::Mat(points));
	cv::Mat rot_mat = cv::getRotationMatrix2D(box.center, box.angle, 1);

	//cv::Mat rotated(src.size(),src.type(),Scalar(255,255,255));
	Mat rotated;
	cv::warpAffine(thr, rotated, rot_mat, src.size(), cv::INTER_CUBIC);

	double myContourAngle = box.angle;
	if (box.size.width < box.size.height) {
		myContourAngle = myContourAngle - 90;
	}

	cout << myContourAngle;

	imshow("image", src);
	imshow("rotated", rotated);




	waitKey(0);
	return 0;
}