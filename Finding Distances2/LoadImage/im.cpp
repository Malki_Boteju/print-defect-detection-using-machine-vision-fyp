#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

using namespace cv;
using namespace std;

Mat src,src_gray;
int thresh = 100;
int max_thresh = 255;
int flag = 0;
RNG rng(12345);
vector<vector<Point> > contours;
vector<float> intercontour_distances;
float contour_center, distance_origin, distance_center,contour_size;
Point cloth_center;

/// Function header
void thresh_callback(int, void*);
void euc_distance(vector<vector<Point> > contours,Mat drawing);
Point find_cloth_center();
void find_angle(vector<vector<Point> > contours);

/** @function main */
int main(int argc, char** argv)
{
	/// Load source image and convert it to gray
	src = imread(argv[1], 1);


	/// Convert image to gray and blur it
	cvtColor(src, src_gray, CV_BGR2GRAY);
	blur(src_gray, src_gray, Size(3, 3));

	/// Create Window
	char* source_window = "Source";
	namedWindow(source_window, CV_WINDOW_AUTOSIZE);
	imshow(source_window, src);

	cloth_center = find_cloth_center();


	createTrackbar(" Canny thresh:", "Source", &thresh, max_thresh, thresh_callback);
	thresh_callback(0, 0);



	waitKey(0);
	return(0);
}

/** @function thresh_callback */
void thresh_callback(int, void*)
{
	Mat canny_output;

	vector<Vec4i> hierarchy;

 	

	/// Detect edges using canny
	Canny(src_gray, canny_output, thresh, thresh * 2, 3);
	/// Find contours
	findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	vector<double> distances((contours.size() / 2)*(contours.size() - 1));

	/// Get the moments
	vector<Moments> mu(contours.size());
	for (int i = 0; i < contours.size(); i++)
	{
		mu[i] = moments(contours[i], false);
	}

	///  Get the mass centers:
	vector<Point2f> mc(contours.size());
	for (int i = 0; i < contours.size(); i++)
	{
		mc[i] = Point2f(mu[i].m10 / mu[i].m00, mu[i].m01 / mu[i].m00);
	}

	/// Draw contours
	Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);

	printf("\t Info: Area and Contour Length \n");
	for (int i = 0; i < contours.size(); i++)
	{
		if (i % 2 == 0) {
			printf(" \n* Contour[%d] - Area (M_00) = %.2f - Area OpenCV: %.2f - Length: %.2f", i, mu[i].m00, contourArea(contours[i]), arcLength(contours[i], true));
			cout << " - center:" << mc[i];
			Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, Point());
			circle(drawing, mc[i], 4, color, -1, 8, 0);
		}
	}
	euc_distance(contours, drawing);
	cout << CV_VERSION;

	/// Show in a window
	namedWindow("Contours", CV_WINDOW_AUTOSIZE);
	imshow("Contours", drawing);


	find_angle(contours);
	/// Calculate the area with the moments 00 and compare with the result of the OpenCV function
	//printf("\t Info: Area and Contour Length \n");
	//for (int i = 0; i < contours.size(); i++)
	//{
	//	if (i%2 ==0) {
	//		printf(" \n* Contour[%d] - Area (M_00) = %.2f - Area OpenCV: %.2f - Length: %.2f", i, mu[i].m00, contourArea(contours[i]), arcLength(contours[i], true));
	//		cout << " - center:" << mc[i];
	//		Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
	//		drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, Point());
	//		circle(drawing, mc[i], 4, color, -1, 8, 0);
	//	}
	//}


}

///** @function euc_distance*/
void euc_distance(vector<vector<Point> > contours,Mat drawing) {

	for (int i = 0; i < contours.size(); i++) {
		
		cout << "\n---------------------------------------contour " << i;
		
			// use contours[i] for the current contour
		if (i %2== 0) {  // **************************************************************************************************change larer
			//******************************************************************************************************************
			for (int j = 0; j < contours[i].size(); j++) {
				distance_origin = sqrt(pow((0 - contours[i][j].x), 2) + pow((0 - contours[i][j].y), 2));  // distance to the contour center from (0,0)
				distance_center = sqrt(pow((cloth_center.x - contours[i][j].x), 2) + pow((cloth_center.y - contours[i][j].y), 2));  // distance to the contour center from cloth center

				
				cout << "\n" << distance_center << " "<< distance_origin ;
				//cout << "\n" << sqrt(pow((0 - contours[i][j].x), 2) + pow((0 - contours[i][j].y), 2));
				//line(drawing, Point(0, 0), contours[2][j], Scalar(255, 255, 255), 1, 8, 0);
	
			}
		}
		
	}
}


//find the center of the cloth
Point find_cloth_center() {
	
	Mat thr;
	threshold(src_gray, thr, 100, 255, THRESH_BINARY);

	Mat thrCopy = thr.clone();

	int x_min = thr.cols, x_max = 0, y_min = thr.rows, y_max = 0;
	int r, c;
	for (r = 0; r < thr.rows; ++r)
	{
		for (c = 0; c < thr.cols; ++c)
		{
			if (thr.at<uchar>(r, c) == 0)
			{
				if (r < y_min)
					y_min = r;
				if (r > y_max)
					y_max = r;
				if (c < x_min)
					x_min = c;
				if (c > x_max)
					x_max = c;
			}
		}
	}



	cv::Point middle((int)(x_min + (x_max - x_min) / 2), (int)(y_min + (y_max - y_min) / 2));
	return middle;
}

//find boundingboxes of the contours
void find_angle(vector<vector<Point> > contours) {

	Mat threshold_output;
	vector<vector<Point> > contours_poly(contours.size());
	vector<RotatedRect> minRect(contours.size());
	vector<Point2f>center(contours.size());
	vector<float>radius(contours.size());

	for (int i = 0; i < contours.size(); i++)
	{
		if (i%2 ==0) {
			minRect[i] = minAreaRect(Mat(contours[i]));
		}
	}
	threshold(src_gray, threshold_output, thresh, 255, THRESH_BINARY);

	/// Draw polygonal contour + bonding rects + circles

	//---------------------------------------------------------------------------check the documentation for angle
	///https://namkeenman.wordpress.com/2015/12/18/open-cv-determine-angle-of-rotatedrect-minarearect/????????????????????????
	Mat drawing = Mat::zeros(threshold_output.size(), CV_8UC3);
	for (int i = 0; i < contours.size(); i++)
	{
		if (i % 2 == 0) {
			Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			// contour
			drawContours(drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point());
			// rotated rectangle
			Point2f rect_points[4]; minRect[i].points(rect_points);
			for (int j = 0; j < 4; j++)
				line(drawing, rect_points[j], rect_points[(j + 1) % 4], color, 1, 8);
			double myContourAngle = minRect[i].angle;
			if (minRect[i].size.width < minRect[i].size.height) {
				myContourAngle = myContourAngle - 90;
			}

			cout << "\n angle :"<<myContourAngle;
		}
	}

	/// Show in a window
	namedWindow("Contours", CV_WINDOW_AUTOSIZE);
	imshow("Contours", drawing);
}